﻿using System;
using DbUpDemoApp.Configuration;
using DbUpDemoApp.Services;

namespace DbUpDemoApp.Startup;

public class Application : IApplication
{
    private readonly IUpgradeDatabaseService _upgradeDatabaseService;

    public Application(IUpgradeDatabaseService upgradeDatabaseService)
    {
        _upgradeDatabaseService = upgradeDatabaseService;
    }

    public int Run(string[] args)
    {
        try
        {
            var environment = ArgumentParser.ParseEnvironmentArgs(args);
            var action = ArgumentParser.ParseDatabaseActionArgs(args);

            if (action == DatabaseAction.UpgradeDb)
            {
                var exitCode = _upgradeDatabaseService.PerformDbUpgrade(environment);
                return exitCode;
            }

            Console.WriteLine("Nothing to do");
            return ExitCode.Success;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return ExitCode.Error;
        }
    }
}