﻿namespace DbUpDemoApp.Startup;

public interface IApplication
{
    int Run(string[] args);
}