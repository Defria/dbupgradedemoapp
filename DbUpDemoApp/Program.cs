﻿using Autofac;
using DbUpDemoApp.IoC;
using DbUpDemoApp.Startup;

namespace DbUpDemoApp;

internal class Program
{
    // DbUpDemoApp.exe upgradeDb:development 
    // DbUpDemoApp.exe restoreDb:development 
    private static int Main(string[] args)
    {
        var register = Registration.Register();
        var app = register.Resolve<Application>();
        var exitCode = app.Run(args);
        return exitCode;
    }
}