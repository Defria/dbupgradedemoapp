﻿using System.Linq;
using System.Reflection;
using Autofac;

namespace DbUpDemoApp.IoC;

public static class Registration
{
    public static IContainer Register()
    {
        var assembly = Assembly.Load(nameof(DbUpDemoApp));

        var interfaces = assembly.GetTypes()
            .Where(t => t.Namespace != null && t.Namespace.StartsWith($"{nameof(DbUpDemoApp)}.") && t.IsInterface);

        var builder = new ContainerBuilder();
        builder.RegisterAssemblyTypes(assembly)
            .Where(t => t.IsClass && interfaces.Any(i => i.Name.Contains(t.Name)))
            .As(t => t.GetInterfaces().FirstOrDefault(i => i.Name.Contains(t.Name) && i.IsInterface)).AsSelf();

        var container = builder.Build();
        return container;
    }
}