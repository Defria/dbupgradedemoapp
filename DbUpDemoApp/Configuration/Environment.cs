﻿namespace DbUpDemoApp.Configuration;

public enum Environment
{
    Production,
    Staging,
    Test,
    Development
}