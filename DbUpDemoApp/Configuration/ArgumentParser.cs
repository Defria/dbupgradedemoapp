﻿using System;
using System.ComponentModel;
using System.Linq;

namespace DbUpDemoApp.Configuration;

public static class ArgumentParser
{
    public static Environment ParseEnvironmentArgs(string[] args)
    {
        var arg = ParseArgs(args);
        var environmentArg = arg[1];
        var validEnvironment = Enum.TryParse(environmentArg, true, out Environment environment);
        if (validEnvironment) return environment;

        throw new InvalidEnumArgumentException(GetErrorMessage(environmentArg));
    }

    public static DatabaseAction ParseDatabaseActionArgs(string[] args)
    {
        var arg = ParseArgs(args);
        var actionArg = arg[0];
        var validActionArg = Enum.TryParse(actionArg, true, out DatabaseAction databaseAction);
        if (validActionArg) return databaseAction;

        throw new InvalidEnumArgumentException(GetErrorMessage(actionArg));
    }

    private static string[] ParseArgs(string[] args)
    {
        var errorMessage = GetErrorMessage();
        if (args == null || !args.Any()) throw new ArgumentException(errorMessage);
        var arg = args[0].Split(':');
        if (arg.Length != 2) throw new ArgumentException(errorMessage);
        return arg;
    }

    private static string GetErrorMessage(string arg = "")
    {
        const string upgradeDbExpectedArgFormat = "Expected => upgradeDb:development / upgradeDb:staging / upgradeDb:staging / upgradeDb:production";
        const string restoreDbExpectedArgFormat = "Expected => restoreDb:development / restoreDb:staging / restoreDb:staging";
        return
            $" {upgradeDbExpectedArgFormat} or \n {restoreDbExpectedArgFormat} \n But was {arg}\n\n eg: DbUpDemoApp.exe upgradeDb:development ";
    }
}