﻿namespace DbUpDemoApp.Configuration;

public interface IConfigurationProvider
{
    string GetDatabaseConnectionString(Environment environment);
    string GetScriptsDirectoryPath(Environment environment);
}