﻿namespace DbUpDemoApp.Configuration;

public enum DatabaseAction
{
    UpgradeDb = 1,
    RestoreDb
}