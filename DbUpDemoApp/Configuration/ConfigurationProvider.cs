﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace DbUpDemoApp.Configuration;

public class ConfigurationProvider : IConfigurationProvider
{
    private IConfigurationRoot _configuration;

    public ConfigurationProvider()
    {
        InitializeConfiguration();
    }

    public string GetDatabaseConnectionString(Environment environment)
    {
        return _configuration.GetConnectionString(environment.ToString());
    }

    public string GetScriptsDirectoryPath(Environment environment)
    {
        return _configuration.GetSection($"ScriptsDirectories:{environment}").Value;
    }

    public string GetPostRestoreScriptsDirectoryPath(Environment environment)
    {
        return _configuration.GetSection($"PostRestoreScriptsDirectories:{environment}").Value;
    }

    private void InitializeConfiguration()
    {
        _configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetParent(AppContext.BaseDirectory)?.FullName)
            .AddJsonFile("appsettings.json", false)
            .Build();
    }
}