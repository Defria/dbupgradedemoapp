﻿using System;
using DbUp;
using DbUp.Engine;
using DbUpDemoApp.Configuration;

namespace DbUpDemoApp.Services;

public class DatabaseService : IDatabaseService
{
    public int ExecuteScripts(string dbConnectionString, string pathToScripts)
    {
        var upgradeEngine =
            DeployChanges.To
                .SqlDatabase(dbConnectionString)
                .WithScriptsFromFileSystem(pathToScripts)
                .LogToConsole()
                .Build();
        var result = upgradeEngine.PerformUpgrade();
        if (!result.Successful)
        {
            WriteErrorMessage(result);
            return ExitCode.Error;
        }

        WriteSuccessMessage();
        return ExitCode.Success;
    }

    private static int WriteSuccessMessage()
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Success!");
        Console.ResetColor();
        return 0;
    }

    private static int WriteErrorMessage(DatabaseUpgradeResult result)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(result.Error);
        Console.ResetColor();
#if DEBUG
        Console.ReadLine();
#endif
        return -1;
    }
}