﻿using System;
using DbUpDemoApp.Configuration;
using Environment = DbUpDemoApp.Configuration.Environment;

namespace DbUpDemoApp.Services;

public class UpgradeDatabaseService : IUpgradeDatabaseService
{
    private readonly IDatabaseService _databaseService;
    private readonly IConfigurationProvider _provider;

    public UpgradeDatabaseService(IConfigurationProvider provider, IDatabaseService databaseService)
    {
        _provider = provider;
        _databaseService = databaseService;
    }

    public int PerformDbUpgrade(Environment environment)
    {
        try
        {
            var result = PerformUpgrade(environment);

            return result;
        }
        catch (Exception)
        {
            return ExitCode.Error;
        }
    }

    private int PerformUpgrade(Environment environment)
    {
        var dbConnectionString = _provider.GetDatabaseConnectionString(environment);
        var scriptsDirectory = _provider.GetScriptsDirectoryPath(environment);
        var exitCode = _databaseService.ExecuteScripts(dbConnectionString, scriptsDirectory);
        return exitCode;
    }
}