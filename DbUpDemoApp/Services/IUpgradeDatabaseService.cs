﻿using DbUpDemoApp.Configuration;

namespace DbUpDemoApp.Services;

public interface IUpgradeDatabaseService
{
    int PerformDbUpgrade(Environment environment);
}