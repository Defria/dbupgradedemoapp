﻿namespace DbUpDemoApp.Services;

public interface IDatabaseService
{
    int ExecuteScripts(string dbConnectionString, string pathToScripts);
}