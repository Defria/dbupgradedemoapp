#  DbUpDemoApp

> Quote: DbUp is a .NET library that helps you to deploy changes to SQL Server databases. It tracks which SQL scripts have been run already, and runs the change scripts that are needed to get your database up to date.  
You can learn more about the tool here [DBUp](https://github.com/DbUp/DbUp)  

This a Dotnet Core 2.1 Utility tool wrapping the features provided by DbUp. You can add this to your CI/CD pipeline

### Publishing artifacts for deployment

> Execute the below commands to publish artifacts for the respective OS  
```
dotnet publish -c Release -r win10-x64  
dotnet publish -c Release -r ubuntu.16.10-x64
```
