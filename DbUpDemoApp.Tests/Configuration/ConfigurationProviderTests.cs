using System;
using DbUpDemoApp.Configuration;
using Xunit;
using Environment = DbUpDemoApp.Configuration.Environment;

namespace DbUpDemoApp.Tests.Configuration;

// dotnet test --filter Category=Integration
// https://www.jetbrains.com/help/resharper/Test_Categories.html
public class ConfigurationProviderTests
{
    [Theory]
    [Trait("Category", "Integration")]
    [InlineData(Environment.Production, "Prod")]
    [InlineData(Environment.Staging, "Staging")]
    [InlineData(Environment.Test, "Test")]
    [InlineData(Environment.Development, "Dev")]
    public void GetScriptFilePath_WhenCalledWithEnvironment_PathShouldBeSet(Environment environment,
        string expectedEnvString)
    {
        // Arrange
        var config = new ConfigurationProvider();

        // Act
        var scriptsDirectory = config.GetScriptsDirectoryPath(environment);

        // Assert
        Assert.NotNull(scriptsDirectory);
        Assert.Contains(expectedEnvString, scriptsDirectory, StringComparison.CurrentCultureIgnoreCase);
    }

    [Theory]
    [Trait("Category", "Integration")]
    [InlineData(Environment.Production, "DB01")]
    [InlineData(Environment.Staging, "DB02")]
    [InlineData(Environment.Test, "DB03")]
    [InlineData(Environment.Development, "DB04")]
    public void GetDatabaseConnectionString_WhenCalledWithEnvironment_ConnectionStringShouldBeSet(
        Environment environment, string dbName)
    {
        // Arrange
        var config = new ConfigurationProvider();

        // Act
        var connectionString = config.GetDatabaseConnectionString(environment);

        // Assert
        Assert.NotNull(connectionString);
        Assert.Contains(dbName, connectionString, StringComparison.CurrentCultureIgnoreCase);
    }

    [Theory]
    [Trait("Category", "Integration")]
    [InlineData(Environment.Staging, "staging")]
    [InlineData(Environment.Test, "test")]
    [InlineData(Environment.Development, "dev")]
    public void GetPostRestoreScriptsDirectoryPath_WhenCalledWithEnvironment_ShouldReturnDirectoryPath(
        Environment environment, string envStringValue)
    {
        // Arrange
        var config = new ConfigurationProvider();

        // Act
        var connectionString = config.GetPostRestoreScriptsDirectoryPath(environment);

        // Assert
        Assert.NotNull(connectionString);
        Assert.Contains(envStringValue, connectionString, StringComparison.CurrentCultureIgnoreCase);
    }
}