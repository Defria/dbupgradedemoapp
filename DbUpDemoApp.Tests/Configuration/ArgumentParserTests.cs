﻿using System;
using System.ComponentModel;
using DbUpDemoApp.Configuration;
using Xunit;
using Environment = DbUpDemoApp.Configuration.Environment;

namespace DbUpDemoApp.Tests.Configuration;

public class ArgumentParserTests
{
    [Theory]
    [Trait("Category", "Unit")]
    [InlineData("env:production", Environment.Production)]
    [InlineData("env:staging", Environment.Staging)]
    [InlineData("env:test", Environment.Test)]
    [InlineData("env:development", Environment.Development)]
    public void ParseEnvironmentArgs_WhenCalledWithValidArguments_ShouldReturnMatchingEnvironment(string argument,
        Environment environment)
    {
        // Arrange
        var environmentArgs = new[] { argument };

        // Act
        var result = ArgumentParser.ParseEnvironmentArgs(environmentArgs);

        // Assert
        Assert.IsType<Environment>(result);
        Assert.True(result == environment);
    }

    [Fact]
    [Trait("Category", "Unit")]
    public void ParseEnvironmentArgs_WhenCalledWithInvalidArguments_ShouldThrowException()
    {
        // Arrange
        var environmentArgs = new[] { "test" };

        // Act
        void Parser()
        {
            ArgumentParser.ParseEnvironmentArgs(environmentArgs);
        }

        // Assert
        Assert.Throws<ArgumentException>(Parser);
    }

    [Fact]
    [Trait("Category", "Unit")]
    public void ParseEnvironmentArgs_WhenCalledWithInvalidArgumentsMatchingPattern_ShouldThrowException()
    {
        // Arrange
        var environmentArgs = new[] { "test:foo" };

        // Act
        void Parser()
        {
            ArgumentParser.ParseEnvironmentArgs(environmentArgs);
        }

        // Assert
        Assert.Throws<InvalidEnumArgumentException>(Parser);
    }

    [Fact]
    [Trait("Category", "Unit")]
    public void ParseDatabaseActionArgs_WhenCalledWithValidArgumentRestoreDb_ShouldReturnDatabaseActionRestoreDb()
    {
        // Arrange
        var args = new[] { "restoreDb:development" };

        // Act
        var result = ArgumentParser.ParseDatabaseActionArgs(args);

        // Assert
        Assert.IsType<DatabaseAction>(result);
        Assert.True(result == DatabaseAction.RestoreDb);
    }


    [Fact]
    [Trait("Category", "Unit")]
    public void ParseDatabaseActionArgs_WhenCalledWithArgumentUpgradeDb_ShouldReturnDatabaseActionUpgradeDb()
    {
        // Arrange
        var args = new[] { "upgradeDb:development" };

        // Act
        var result = ArgumentParser.ParseDatabaseActionArgs(args);

        // Assert
        Assert.IsType<DatabaseAction>(result);
        Assert.True(result == DatabaseAction.UpgradeDb);
    }


    [Fact]
    [Trait("Category", "Unit")]
    public void ParseDatabaseActionArgs_WhenCalledWithInvalidArgumentsMatchingPattern_ThrowException()
    {
        // Arrange
        var args = new[] { "foo:bared" };

        // Act
        Action act = () => ArgumentParser.ParseDatabaseActionArgs(args);

        // Assert
        Assert.Throws<InvalidEnumArgumentException>(act);
    }


    [Fact]
    [Trait("Category", "Unit")]
    public void ParseDatabaseActionArgs_WhenCalledWithInvalidArguments_ThrowArgumentException()
    {
        // Arrange
        var args = new[] { "" };

        // Act
        Action act = () => ArgumentParser.ParseDatabaseActionArgs(args);

        // Assert
        Assert.Throws<ArgumentException>(act);
    }


    [Fact]
    [Trait("Category", "Unit")]
    public void ParseDatabaseActionArgs_WhenCalledWithNullArguments_ThrowArgumentException()
    {
        // Act
        Action act = () => ArgumentParser.ParseDatabaseActionArgs(null);

        // Assert
        Assert.Throws<ArgumentException>(act);
    }
}