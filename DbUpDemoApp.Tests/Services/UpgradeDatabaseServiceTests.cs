﻿using System;
using DbUpDemoApp.Configuration;
using DbUpDemoApp.Services;
using Moq;
using Xunit;
using Environment = DbUpDemoApp.Configuration.Environment;

namespace DbUpDemoApp.Tests.Services;

public class UpgradeDatabaseServiceTests
{
    [Fact]
    [Trait("Category", "Unit")]
    public void PerformDbUpgrade_WhenCalledWithValidEnvironment_ExitCodeIsZero()
    {
        // Arrage 
        var config = new Mock<IConfigurationProvider>().Object;
        var dbService = new Mock<IDatabaseService>().Object;
        var updateService = new UpgradeDatabaseService(config, dbService);

        // Act
        var result = updateService.PerformDbUpgrade(It.IsAny<Environment>());

        // Assert
        Assert.True(result == 0);
    }


    [Fact]
    [Trait("Category", "Unit")]
    public void PerformDbUpgrade_WhenCalledThrowsException_ExitCodeIsNotZero()
    {
        // Arrange 
        var config = new Mock<IConfigurationProvider>().Object;
        var dbService = new Mock<IDatabaseService>();
        dbService.Setup(m => m.ExecuteScripts(It.IsAny<string>(), It.IsAny<string>())).Throws<Exception>();
        var updateService = new UpgradeDatabaseService(config, dbService.Object);

        // Act
        var result = updateService.PerformDbUpgrade(It.IsAny<Environment>());

        // Assert
        Assert.True(result != 0);
    }
}