﻿using DbUpDemoApp.Services;
using DbUpDemoApp.Startup;
using Moq;
using Xunit;

namespace DbUpDemoApp.Tests.Startup;

public class ApplicationTests
{
    [Fact]
    [Trait("Category", "Unit")]
    public void Run_WhenCalledWithValidUpgradeDbArguments_ReturnsExitCodeZero()
    {
        // Arrange
        var dbUpgradeService = new Mock<IUpgradeDatabaseService>();
        var app = new Application(dbUpgradeService.Object);

        // Act
        var result = app.Run(new[] { "upgradeDb:development" });

        // Assert
        Assert.True(result == 0);
    }

    [Fact]
    [Trait("Category", "Unit")]
    public void Run_WhenCalledWithValidRestoreDbArguments_ReturnsExitCodeZero()
    {
        // Arrange
        var dbUpgradeService = new Mock<IUpgradeDatabaseService>();
        var app = new Application(dbUpgradeService.Object);


        // Act
        var result = app.Run(new[] { "restoreDb:development" });

        // Assert
        Assert.True(result == 0);
    }


    [Fact]
    [Trait("Category", "Unit")]
    public void Run_WhenCalledWithInValidArguments_ReturnsExitCodeIsNotZero()
    {
        // Arrange
        var dbUpgradeService = new Mock<IUpgradeDatabaseService>();
        var app = new Application(dbUpgradeService.Object);


        // Act
        var result = app.Run(new[] { $"{It.IsAny<string>()}" });

        // Assert
        Assert.True(result != 0);
    }


    [Fact]
    [Trait("Category", "Unit")]
    public void Run_WhenCalledWithInValidArgumentsMatchingPattern_ReturnsExitCodeIsNotZero()
    {
        // Arrange
        var dbUpgradeService = new Mock<IUpgradeDatabaseService>();
        var app = new Application(dbUpgradeService.Object);


        // Act
        var result = app.Run(new[] { "foobar:foobar" });

        // Assert
        Assert.True(result != 0);
    }
}